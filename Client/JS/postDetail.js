// get all query params from url
const urlParams = new URLSearchParams(window.location.search);
// get query param postId  from objecte URLSearchParams
const postId = urlParams.get("postId");

const user = JSON.parse(localStorage.getItem("user"));
/* === get post === */
function getPost() {
  // selector posts div to append post
  const postsDom = document.querySelector(".posts");
  // get token  from  localStorage
  const token = localStorage.getItem("token");
  // get post by axios
  // call toggleLodear to Waiting until the response (then) is done
  toggleLodear(true);
  axios
    .get(`http://localhost:5000/api/v1/posts/post/${postId}`, {
      headers: { authorization: localStorage.getItem("token") },
    })
    .then((response) => {
      // call toggleLodear function to hide lodear because the response (then) is done
      toggleLodear(false);
      // declaration of post
      const post = response.data;
      // const check like
      const checkLike = post.likes.find((like) => like === user._id);

      postsDom.innerHTML = ` 
            <!-- Start Post -->
              <h1>${post.owner.username} post</h1>
              <div class="card shadow my-3 ">
                <div class="card-header post-detail">
                    <img
                    src="${post.owner.image.url}"
                    alt="user"
                        class="img-user mx-2 rounded-circle border border-2"
                    />
                    <b>@${post.owner.username}</b>
                  
                </div>

                <div class="card-body">
                  <img
                    src="${post.image.url}"
                    class="w-100 post-img"
                    alt="post-img"
                    />
                    <h6 class="mt-2 time-ago">${post.createdAt.slice(
                      0,
                      10
                    )}</h6>
                     <h5 class="">${post.title}</h5>
                    <p class="card-text">
                    ${post.description.length}
                     </p>

                  <hr />
                 

                 <div class="card-action">
                 <i class="${
                   checkLike
                     ? "fa fa-thumbs-up like-icon"
                     : "fa fa-thumbs-up unlike-icon"
                 }"  aria-hidden="true" onclick="toogleLike('${encodeURIComponent(
        JSON.stringify(post._id)
      )}')" ></i>
                 <span class="show-comments" onclick="showComments()">(${
                   post.likes.length
                 })</span> 
                
                    <i class="fa-solid fa-pen"></i>
                    <span class="show-comments" onclick="showComments()">(${
                      post.comments.length
                    })comments</span>
                    <span id="tags-post-${post._id}">   </span>
                </div>


                    <div class="comments-post ">
                      <div class="comments-post-show">
                          
                     </div>
                     ${
                       token
                         ? `    <div class="add-comment">
                     <input
                       type="text"
                       class="form-control add-comment-input"
                       id="formGroupExampleInput2"
                       placeholder="add comment"
                     />
                     <button type="submit" class="add-comment-btn" onclick="addComment('${encodeURIComponent(
                       JSON.stringify(post._id)
                     )}')">
                       add comment
                     </button>
          
                   </div>
            
               </div>`
                         : ""
                     }
                 
                    
                 </div>
              
              </div>
            <!-- End Post -->`;

      // add tags exemple

      const tags = post.category;

      const tagPostId = `tags-post-${post._id}`;
      const psotTags = document.getElementById(tagPostId);

      tags.forEach(
        (tag) =>
          (psotTags.innerHTML += ` <button type="button" class="btn btn-secondary ">${tag}</button>`)
      );

      // add comments
      const commmentsPost = post.comments.length ? post.comments : [];
      commmentsPost.forEach((comment) => {
        const checkOwenComment = comment.owner === user._id;
        document.querySelector(".comments-post-show").innerHTML += `
        <div class="comments-info">
              <div class="comments-body"> 
                <img
                src="${comment.image}"
                alt="user"
                class="img-user-comment mx-2 rounded-circle border border-2 my-3"
                />
                <div class="user-comment" > 
                <h5 >${comment.username}</h5> 
                <p>${comment.text}</p> 
                </div>

             </div>
               ${
                 checkOwenComment
                   ? ` <button type="submit" class="btn btn-danger btn-delete-comment"onclick="deleteCommet('${encodeURIComponent(
                       JSON.stringify(comment._id)
                     )}')">
            delete
          </button>`
                   : ""
               }
            
        </div>
        
        `;
      });
    });
}
getPost();

/* =====showComments===== */
function showComments() {
  if (document.querySelector(".comments-post").style.display === "none") {
    document.querySelector(".comments-post").style.display = "block";
  } else {
    document.querySelector(".comments-post").style.display = "none";
  }
}
/* =====// showComments //===== */

/* =====addComment===== */
function addComment(postObject) {
  const PostId = JSON.parse(decodeURIComponent(postObject));
  console.log(PostId);
  // get body Comment value from input comment
  const text = document.querySelector(".add-comment-input").value;
  console.log(text);
  // get token  from  localStorage
  const token = localStorage.getItem("token");
  // declaration of headers
  const headers = {
    authorization: `${token}`,
  };

  axios
    // post comment by axios and send body params and headers.
    .post(
      `http://localhost:5000/api/v1/comment/`,
      { PostId, text },
      {
        headers,
      }
    )
    // promise fulfilled ( resolve )
    .then((response) => {
      // call function getPosts for show the new comment
      getPost();
      // call  showAlert for show success add comment message
      showAlert(response.data.message, "success");
    })
    // promise rejected ( reject )
    .catch((errors) => {
      // call  showAlert for show errors message
      showAlert(errors.response?.data.message, "danger");
      console.log(errors.response?.data.message);
      console.log(errors);
    });
}
/* =====// addComment //===== */

/* =====addComment===== */
function deleteCommet(postObject) {
  const CommentId = JSON.parse(decodeURIComponent(postObject));

  // get token  from  localStorage
  const token = localStorage.getItem("token");
  // declaration of headers
  const headers = {
    authorization: `${token}`,
  };

  axios
    // post comment by axios and send body params and headers.
    .delete(`http://localhost:5000/api/v1/comment/${CommentId}`, {
      headers,
    })
    // promise fulfilled ( resolve )
    .then((response) => {
      // call function getPosts for show the new comment
      getPost();
      // call  showAlert for show success add comment message
      showAlert(response.data.message, "success");
    })
    // promise rejected ( reject )
    .catch((errors) => {
      // call  showAlert for show errors message
      showAlert(errors.response?.data.message, "danger");
    });
}
/* =====// addComment //===== */

const toogleLike = async (postObject) => {
  try {
    const PostId = JSON.parse(decodeURIComponent(postObject));
    const response = await axios.put(
      `http://localhost:5000/api/v1/posts/like/${PostId}`,
      postId,
      { headers: { authorization: localStorage.getItem("token") } }
    );
    getPost();
    showAlert(response.data.message, "success");
  } catch (error) {
    showAlert(error.response?.data.message, "danger");
  }
};
