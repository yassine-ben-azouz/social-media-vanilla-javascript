/****====== Login Function ========****/
function login(e) {
  // cancels the event if it is cancelable
  e.preventDefault();
  // take username value from input
  const email = document.querySelector(".email").value;
  // take password value from input
  const password = document.querySelector(".password").value;
  // call toggleLodear to Waiting until the response (then) is done
  toggleLodear(true);
  axios
    .post(`http://localhost:5000/api/v1/users/login`, { email, password })
    .then((response) => {
      console.log(response.data);
      // call successAlert function
      showAlert(`           logged in successfully   `, "success");
      // set user into localStorage
      localStorage.setItem("user", JSON.stringify(response.data.user));
      // set token of user into localStorage
      localStorage.setItem("token", response.data.token);
      // attend 2s and go to home page.
      setTimeout(() => {
        // go to home page
        window.location = "home.html";
      }, 2000);
    })
    .catch((error) => {
      // Alert error username and password
      showAlert(
        `
  ${error.response.data.message}

  `,
        "danger"
      );
    })
    .finally(() => {
      // call toggleLodear function to hide lodear because the response (then) is done
      toggleLodear(false);
    });
}
// login btn DOM
const btnLogin = document.querySelector(".btn-login");
// addEventListener to button of login
btnLogin.addEventListener("click", login);
