// declaration of lastPage
let lastPage = 1;

/***** GET ALL Posts ****/
function getPosts(currentPage = 1, relaod = true) {
  // select the div name posts (the parent of post)
  const postsDom = document.querySelector(".posts");
  if (relaod) {
    // fill page home from posts
    postsDom.innerHTML = ``;
  }

  // get user logged from localStorage
  const user = JSON.parse(localStorage.getItem("user"));
  // call toggleLodear to Waiting until the response (then) is done
  toggleLodear(true);
  // get posts by axios and send page like a query params
  axios
    .get(`http://localhost:5000/api/v1/posts/${currentPage}`)
    .then((response) => {
      // call toggleLodear function to hide lodear because the response (then) is done
      toggleLodear(false);

      // declartaion of posts (get posts from response object)
      const posts = response.data.posts;
      console.log(response.data);
      // assignment lastPage to new value
      lastPage = response.data.pagesCount;
      posts.forEach((post) => {
        // check post cliked (choosen) is my post and user log in
        const MyPost = user != null && user._id == post.owner._id;
        const postOwner = post.owner._id;
        console.log(postOwner);
        postsDom.innerHTML += ` 
          <!-- Start Post -->
            <div class="card shadow mt-3 card-details" >
              <div class="card-header post-header d-flex " >
                <span class="user-profile-btn" onclick="goToProfileUser('${encodeURIComponent(
                  JSON.stringify(post.owner._id)
                )}')"
                >
                  <img
                    src="${post.owner.image?.url}"
                    alt="user"
                    class="img-user mx-2 rounded-circle border border-2"
                  />
                  <b >
                    @${post.owner.username} 
                  </b>
                </span>
                ${
                  MyPost
                    ? `   <button  class="btn btn-primary  btn-edit-post"  data-bs-toggle="modal" 
                data-bs-target="#edit-post" 
                onclick="getEDitPost('${encodeURIComponent(
                  JSON.stringify(post)
                )}')"
                     >
                    edit
                </button>
                <button class="btn btn-danger mx-2"   data-bs-toggle="modal"
                        data-bs-target="#delete-post" onclick="deletePost('${encodeURIComponent(
                          JSON.stringify(post)
                        )}')"> delete </button>
                
                `
                    : ""
                }
             
               
              </div>
              <div class="card-body" onclick="postDetail('${encodeURIComponent(
                JSON.stringify(post._id)
              )}')"" >
                <img
                  src="${post.image.url}"
                  class="w-100 post-img"
                  alt="post-img"
                />
                <h6 class="mt-2 time-ago">${post.createdAt.slice(0, 10)}</h6>
                <h5 class="">${post.title !== null ? post.title : "title"}</h5>
      
                <p class="card-text">
                 ${
                   post.description.length
                     ? post.description
                     : ` With supporting text below as a natural lead-in to 
                          additional content.`
                 }
               
                </p>
                <hr />
                <div class="comment">
                <i class="fa fa-thumbs-up unlike-icon"
                 aria-hidden="true"></i>
                <span class="show-comments" onclick="showComments()">(${
                  post.likes.length
                })</span> 
                  <i class="fa-solid fa-pen"></i>
                  <span>(${post.comments.length})comments</span>
                  <span id="tags-post-${post._id}">   </span>
                
                </div>
              </div>

             
            </div>
          <!-- End Post -->`;
        // add tags exemple
        const tags = post.category;

        const tagPostId = `tags-post-${post._id}`;
        const psotTags = document.getElementById(tagPostId);

        tags.forEach(
          (tag) =>
            (psotTags.innerHTML += ` <button type="button" class="btn btn-secondary ">${tag}</button>`)
        );
      });
      // get token from localStorage
      const token = localStorage.getItem("token");
      // check user is logged or not to show button add post
      token
        ? (document.querySelector(".add-new-post").style.display = "block")
        : document
            .querySelector(".add-new-post")
            .setAttribute("style", "display:none !important");
    })
    .catch((errors) => {
      console.log(errors);
    });
}

getPosts();

/***** Create New Posts ****/
function addPost() {
  // get title input value
  const title = document.querySelector(".add-title-post").value;
  // get body input value
  const description = document.querySelector(".add-body-post").value;
  // get category input value
  const category = document.querySelector(".add-category-post").value;
  // get image input file
  const image = document.querySelector(".add-image-post").files[0];
  // declared form data (because is there image file)
  let formData = new FormData();
  formData.append("title", title);
  formData.append("description", description);
  formData.append("image", image);
  formData.append("category", category);

  // get  token from localStorage
  const token = localStorage.getItem("token");
  // set  token to headers
  const headers = {
    authorization: `${token}`,
  };
  // call toggleLodear to Waiting until the response (then) is done
  toggleLodear(true);
  axios
    .post(
      `http://localhost:5000/api/v1/posts/
  `,
      formData,
      {
        headers,
      }
    )
    // promise fulfilled ( resolve )
    .then((response) => {
      // call function getPosts for show the new post
      getPosts();
      // Alert success for  add new post
      showAlert(response.data.response.message, "success");
    })
    // promise rejected ( rejecte )
    .catch((error) => {
      // call Alert function to show error message  for operation of add new post
      showAlert(
        error.message?.data.response, // error title message from back (api)

        // color of message Alert (danger = color red)
        "danger"
      );
    })
    .finally(() => {
      // call toggleLodear function to hide lodear because the response (then) is done
      toggleLodear(false);
    });
}
// selector btn add post
const addPostBtn = document.querySelector(".add-post-btn");
// addEventListener to btn add post
addPostBtn.addEventListener("click", addPost);

/* ====== postDetail ===== */
const postDetail = (postObject) => {
  //
  const postId = JSON.parse(decodeURIComponent(postObject));

  // go to postDetail page and send post id as query params
  window.location = `/Client/postDetail.html?postId=${postId}`;
};
/* ======// postDetail //===== */

/* ==== handleInfiniteScroll ===== */
// declaration of currentPage
let currentPage = 1;
const handleInfiniteScroll = () => {
  // declaration of endOfPage (end of page by cursor)
  const endOfPage =
    // Measure the width and height of the page and compare it to the end of the page
    window.innerHeight + window.pageYOffset >= document.body.offsetHeight - 2; // -2 by me for to avoid error pagination

  if (endOfPage && currentPage < lastPage) {
    console.log("currentPage", currentPage);
    // currentPage plus 1 to go to the next page and send currentPage to getPosts like an agrument for show current page.
    getPosts(++currentPage, false);
  }
};
/* ====// handleInfiniteScroll //===== */

window.addEventListener("scroll", handleInfiniteScroll);
