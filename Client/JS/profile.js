// get all query params from url
const urlParams = new URLSearchParams(window.location.search);
// get query param postId  from objecte URLSearchParams
const userId = urlParams.get("userId");
/*=== get user ===*/
function getUser() {
  // selecte profile dom dive
  const profileDom = document.querySelector(".profile-info");
  profileDom.innerHTML = ``;

  // get user log in from the localStorage
  const userLogged = localStorage.getItem("user")
    ? JSON.parse(localStorage.getItem("user"))
    : "";
  // get token from the localStorage
  const token = localStorage.getItem("token");
  // get user info by axios

  axios
    .get(`http://localhost:5000/api/v1/users/${userId}`, {
      headers: { authorization: token },
    })
    // resolve
    .then((response) => {
      console.log(response.data);
      // get user info from response
      const userProfile = response.data;
      // set user info to profile dom div
      console.log(userProfile);

      profileDom.innerHTML += `
      <div class="wrapper my-5">
      <div class="left">
        <img src="${userProfile.image.url}" alt="user" width="100" />

              <input
              type="file"
              class="input-profile-photo"
                />    
              <button class="btn-profile-photo"  onclick="udatePhotoProfie()" >
          upload
        </button>
        <h4>${userProfile.username}</h4>
        <p>UI Developer</p>
      </div>
      <div class="right">
        <div class="info">
          <h3>Information</h3>
          <div class="info_data">
            <div class="data">
              <h4>Email</h4>
              <p>${userProfile.email}</p>
            </div>
            <div class="data">
              <h4>Phone</h4>
              <p>0001-213-998761</p>
            </div>
          </div>
        </div>

        <div class="projects">
          <h3>Projects</h3>
          <div class="projects_data">
            <div class="data">
              <h4>Recent</h4>
              <p>Lorem ipsum dolor sit amet.</p>
            </div>
            <div class="data">
              <h4>Most Viewed</h4>
              <p>dolor sit amet.</p>
            </div>
          </div>
        </div>

        <div class="social_media">
          <ul>
            <li>
              <a href="#"><i class="fab fa-facebook-f"></i></a>
            </li>
            <li>
              <a href="#"><i class="fab fa-twitter"></i></a>
            </li>
            <li>
              <a href="#"><i class="fab fa-instagram"></i></a>
            </li>
          </ul>
        </div>
      </div>
    </div>
      `;
    })
    .catch((error) => {
      console.log(error.response);
    });
}
getUser();
const postsDom = document.querySelector(".posts");

/*=== getPostUser ===*/
function getPostUser() {
  // selector posts div to append post
  // get user info from localStorage
  const user = JSON.parse(localStorage.getItem("user"));
  // fill page from post
  postsDom.innerHTML += ``;
  // call toggleLodear to Waiting until the response (then) is done
  toggleLodear(true);
  axios
    .get(`http://localhost:5000/api/v1/users/${userId}`, {
      headers: { authorization: localStorage.getItem("token") },
    })
    .then((response) => {
      // call toggleLodear function to hide lodear because the response (then) is done
      toggleLodear(false);
      console.log(response.data.posts);
      // get posts from response
      const posts = response.data.posts;
      const userInfo = response.data;
      // loop posts and set info posts to profile page
      posts.forEach((post) => {
        // check is post author id = user logged id
        const MyPost = user != null && user._id == post.owner;

        postsDom.innerHTML += ` 

    <h1>${userInfo.username} posts</h1>   
    <div class="card shadow my-3 ">

            <div class="card-header post-detail d-flex">
            <img
            src="${user.image.url}"
            alt="user"
                class="img-user mx-2 rounded-circle border border-2"
            />
           
            <b>@${userInfo.username}</b>
            ${
              MyPost
                ? `   <button  class="btn btn-primary  btn-edit-post"  data-bs-toggle="modal"
            data-bs-target="#edit-post"
            onclick="getEDitPost('${encodeURIComponent(JSON.stringify(post))}')"
                 >
                edit
            </button>
            <button class="btn btn-danger mx-2"   data-bs-toggle="modal"
                    data-bs-target="#delete-post" onclick="deletePost('${encodeURIComponent(
                      JSON.stringify(post)
                    )}')"> delete </button>
            
            `
                : ""
            }
            
           </div>

           <div class="card-body">
                <img
                src="${post.image.url}"
                class="w-100 post-img"
                alt="post-img"
                />
                <h6 class="mt-2 time-ago">${post.createdAt.slice(0, 10)}</h6>
                <h5 class="">${post.title}</h5>
                <p class="card-text">
                ${post.description}
                  </p>
                  <hr />
           </div>

             
              <span id="tags-post-${post._id}">   </span>
         </div>


    </div>
        
        `;
      });
    })
    .catch((error) => console.log(error.response?.data.message));
}
getPostUser();

const udatePhotoProfie = async () => {
  try {
    const image = document.querySelector(".input-profile-photo").files[0];
    console.log(image);
    const form = new FormData();
    form.append("image", image);
    const response = await axios.put(
      `http://localhost:5000/api/v1/users/register/photo/`,
      form,
      { headers: { authorization: localStorage.getItem("token") } }
    );

    getUser();

    showAlert(response.data.message, "success");
  } catch (error) {
    console.log(error.response.data.message);
    showAlert(error.response.data.message, "danger");
  }
};
