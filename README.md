# Social Media Vanilla JavaScript

## Développement d'une application Social Media website :

- Implémentation du module inscription, connexion et authentification.
- Mise en place des Routers de navigation.
- Gestion de pagination des produits.
- Trie des produits par champ
- Développement d’un module de recherche par catégorie.

## Mots Clés: NodeJS, ExpressJS, Mongo, API, JWT, Axios, JavaScript (DOM), Bootstrap, Cloudinary, Git.

## Screenshot

### Register Page :

![Register](https://gitlab.com/yassine-ben-azouz/social-media-vanilla-javascript/-/raw/master/Screenshot/Register%20Page.jpg)

### Login Page :

![Login](https://gitlab.com/yassine-ben-azouz/social-media-vanilla-javascript/-/raw/master/Screenshot/Login%20Page.jpg)

### Home page user not looged in :

![Home](https://gitlab.com/yassine-ben-azouz/social-media-vanilla-javascript/-/raw/master/Screenshot/Home%20Page%20User%20not%20logged%20in.jpg)

### Home page user looged in :

![Home](https://gitlab.com/yassine-ben-azouz/social-media-vanilla-javascript/-/raw/master/Screenshot/Home%20Page%20User%20logged%20in.jpg)

### Profil page :

![Profil](https://gitlab.com/yassine-ben-azouz/social-media-vanilla-javascript/-/raw/master/Screenshot/Profile%20page.jpg)

### Post Detail :

![Post](https://gitlab.com/yassine-ben-azouz/social-media-vanilla-javascript/-/raw/master/Screenshot/Post%20Details.jpg)
