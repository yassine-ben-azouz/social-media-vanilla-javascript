const router = require("express").Router();
const {
  register,
  login,
  uploadPhotoProfil,
  getUserConnectByToken,
  getUserInfoById,
  UpdateUser,
} = require("../Controllers/UsersControllers");
const { upload, storage } = require("../Middlewares/Multer");

const { Authmiddleware } = require("../Middlewares/Authmiddleware");
const { ValidateObjectId } = require("../Middlewares/ValidateObjectId");

router.post("/register", register);
router.post("/login", login);
// get owner info connect by token
router.get("/", Authmiddleware, getUserConnectByToken);
// get user  connect by id
router.get("/:id", Authmiddleware, ValidateObjectId, getUserInfoById);
//update user info
router.put("/", Authmiddleware, UpdateUser);
// add photo profil
router.put(
  "/register/photo",
  Authmiddleware,
  upload.single("image"),
  uploadPhotoProfil
);

module.exports = router;
