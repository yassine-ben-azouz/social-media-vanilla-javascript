//
const {
  Comment,
  ValidateCreateComment,
  ValidateUpdateComment,
} = require("../Models/CommentsModel");
//
const { User } = require("../Models/UsersModel");
const { Post } = require("../Models/PostsModel");

/**
 * @DEC create new comment
 * @params POST /api/v1/comment/
 * @access PRIVATE (user logged in)
 **/
exports.createComment = async (req, res) => {
  try {
    // validate data
    console.log(req.body);
    const { error } = ValidateCreateComment(req.body);
    if (error)
      return res.status(400).json({ message: error.details[0].message });
    // check is post exist
    const { PostId, text } = req.body;
    // check if post exist or not by id
    const existPost = await Post.findById(PostId);
    if (!existPost) return res.status(404).json({ message: "post not fount" });
    // get user connect to get his username
    const findUser = await User.findById(req.userId);
    const CreateComment = await Comment.create({
      PostId,
      owner: req.userId,
      username: findUser.username,
      text,
      image: findUser.image.url,
    });
    res
      .status(201)
      .json({ message: "comment has been created succssfully", CreateComment });
  } catch (error) {
    res.status(500).json(error);
    console.log(error);
  }
};

/**
 * @DEC    Get All comment
 * @params GET /api/v1/admin/comment/
 * @access PRIVATE (admin)
 **/
exports.getAllComments = async (req, res) => {
  try {
    const comments = await Comment.find()
      .populate("PostId")
      .populate("owner", "-password");
    res.status(200).json(comments);
  } catch (error) {
    res.status(500).json(error);
    console.log(error);
  }
};

/**
 * @DEC update  comment
 * @params POST /api/v1/comment/
 * @access PRIVATE  (owner of this comment)
 **/
exports.updateComment = async (req, res) => {
  try {
    // validate data
    const { error } = ValidateUpdateComment(req.body);
    if (error)
      return res.status(400).json({ message: error.details[0].message });
    const { text } = req.body;
    // check if comment exist or not by id
    const existComment = await Comment.findById(req.params.id);
    if (!existComment)
      return res.status(404).json({ message: "comment not fount" });

    if (existComment.owner.toString() !== req.userId)
      return res
        .status(401)
        .json({ message: " unauthorized, you are not owner of this comment" });
    // updated comment
    const UpdateComment = await Comment.findByIdAndUpdate(req.params.id, {
      text,
    });
    res
      .status(201)
      .json({ message: "comment has been updated succssfully", UpdateComment });
  } catch (error) {
    res.status(500).json(error);
    console.log(error);
  }
};

/**
 * @DEC    delete comment
 * @params GET /api/v1/comment/
 * @access PRIVATE (owner of comment or admin)
 **/
exports.deleteComment = async (req, res) => {
  try {
    const choosenComment = await Comment.findById(req.params.id);
    if (!choosenComment)
      return res.status(404).json({ message: "comment not found" });
    if (
      req.userRole === "admin" ||
      choosenComment.owner.toString() === req.userId
    ) {
      const deleteComment = await Comment.findByIdAndDelete(req.params.id);
      res
        .status(200)
        .json({ message: `comment has been deleted successfully` });
    } else {
      return res.status(401).json({ message: "you are not unauthorized" });
    }
  } catch (error) {
    res.status(500).json(error);
    console.log(error);
  }
};
