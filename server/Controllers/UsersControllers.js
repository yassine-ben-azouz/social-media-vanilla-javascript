const {
  User,
  ValdiateRegister,
  ValidateUpdateUser,
} = require("../Models/UsersModel");
const { Post } = require("../Models/PostsModel");
const { Comment } = require("../Models/CommentsModel.js");
const jwt = require("jsonwebtoken");
const {
  uploadPhotoCloudinary,
  RemovePhotoCloudinary,
  RemoveMultiplePhotosCloudinary,
} = require("../utils/cloudinary");
const bcrypt = require("bcryptjs");

/**
 * @DEC register new user
 * @params POST /api/v1/users/register/
 * @access PUBLIC
 **/
exports.register = async (req, res) => {
  try {
    const { username, email, password, role } = req.body;
    if (!username || !email || !password)
      return res
        .status(400)
        .json({ message: "please fill all required fields" });
    const { error } = ValdiateRegister(req.body);
    if (error)
      return res.status(400).json({ message: error.details[0].message });
    const existUser = await User.findOne({ email });
    if (existUser)
      return res.status(400).json({ message: "user already register" });
    const slat = bcrypt.genSaltSync(10);
    const hachPassword = bcrypt.hashSync(password, slat);
    const user = await User.create({
      username,
      email,
      password: hachPassword,
      role,
    });
    console.log("user", user);
    const token = await jwt.sign(
      { sub: user._id, role: user.role },
      process.env.SECRET_KEY
    );
    res
      .status(201)
      .json({ message: `${user.username} register successfully`, token, user });
  } catch (error) {
    res.status(500).json("something wrong");
    console.log(error);
  }
};

/**
 * @DEC logged  user
 * @params POST /api/v1/users/login/
 * @access PUBLIC
 **/
exports.login = async (req, res) => {
  try {
    const { email, password } = req.body;
    if (!email || !password)
      return res
        .status(400)
        .json({ message: "please fill all required fields" });
    const user = await User.findOne({ email });
    if (!user)
      return res.status(400).json({ message: "you should register first" });
    const checkpassword = await bcrypt.compareSync(password, user.password);
    if (!checkpassword)
      return res.status(400).json({ message: "invaild password" });
    const token = await jwt.sign(
      { sub: user._id, role: user.role },
      process.env.SECRET_KEY
    );
    res
      .status(200)
      .json({ message: `${user.username} logged successfully`, token, user });
  } catch (error) {
    res.status(500).json("something wrong");
    console.log(error);
  }
};

/**
 * @DEC insert photo  profil
 * @params PUT /api/v1/users/login/
 * @access PRIVATE owner (user logged in)
 **/
exports.uploadPhotoProfil = async (req, res) => {
  try {
    // check is file image exist or not
    if (!req.file) return res.status(400).json({ message: "insert photo" });

    // call uploadPhotoCloudinary function to upload image in Cloudinary
    const ResultCloudinary = await uploadPhotoCloudinary(
      req.file.path, // file upload
      "social_media_vanilla_javascript_users" // dossier for save photo
    );
    // user chossen to change his image (user logged in)
    const choosenUser = await User.findByIdAndUpdate(req.userId, {
      image: {
        url: ResultCloudinary.secure_url, // url photo
        publicId: ResultCloudinary.public_id, // public id to remove then call RemovePhotoCloudinary function
      },
    });
    // check if user already have image (if true remove old image)
    if (choosenUser.image.publicId !== null) {
      await RemovePhotoCloudinary(choosenUser.image.publicId);
    }

    res
      .status(200)
      .json({ message: "photo profil upload successfully", choosenUser });
  } catch (error) {
    res.status(500).json(error);
    console.log(error);
  }
};

/**
 * @DEC get all user
 * @params GET /api/v1/admin/users/
 * @access PRIVATE admin
 **/

exports.getAllUser = async (req, res) => {
  try {
    const users = await User.find().select("-password").sort({ createdAt: -1 });
    res.status(200).json(users);
  } catch (error) {
    res.status(500).json({ message: "something wrong" });
    console.log(error);
  }
};

/**
 * @DEC get owner (user looged in by token)
 * @params GET /api/v1/users/
 * @access PRIVATE admin
 **/

exports.getUserConnectByToken = async (req, res) => {
  try {
    const users = await User.findById(req.userId)
      .select("-password")
      .populate("posts");
    res.status(200).json(users);
  } catch (error) {
    res.status(500).json({ message: "something wrong" });
    console.log(error);
  }
};

/**
 * @DEC get owner (user looged in)
 * @params GET /api/v1/users/
 * @access PRIVATE admin
 **/

exports.getUserInfoById = async (req, res) => {
  try {
    const users = await User.findById(req.params.id)
      .select("-password")
      .populate("posts");
    res.status(200).json(users);
  } catch (error) {
    res.status(500).json({ message: "something wrong" });
    console.log(error);
  }
};

/**
 * @DEC udate owner info
 * @params POST /api/v1/users/register/
 * @access PRIVATE (owner )
 **/
exports.UpdateUser = async (req, res) => {
  try {
    // check user exist or not
    const existUser = await User.findById(req.userId);
    if (!existUser) res.status(404).json({ message: "user not found" });
    // validate data from client
    const { username, password } = req.body;
    if (!username || !password)
      return res
        .status(400)
        .json({ message: "please fill all required fields" });

    const { error } = ValidateUpdateUser(req.body);
    if (error)
      return res.status(400).json({ message: error.details[0].message });

    // hach password
    const slat = bcrypt.genSaltSync(10);
    const hachPassword = bcrypt.hashSync(password, slat);
    // update user
    const newUser = await User.findByIdAndUpdate(req.userId, {
      username,
      password: hachPassword,
    });
    // send response to client
    res
      .status(200)
      .json({ message: `${newUser.username} update successfully` });
  } catch (error) {
    res.status(500).json("something wrong");
    console.log(error);
  }
};

/**
 * @DEC delete  all users
 * @params POST /api/v1/admin/users/
 * @access PRIVATE admin
 **/
exports.deleteAllUsers = async (req, res) => {
  try {
    const deleteAllUsers = await User.deleteMany();
    res
      .status(200)
      .json({ message: "all users has been deleted successfully" });
  } catch (error) {
    res.status(500).json("something wrong");
    console.log(error);
  }
};

/**
 * @DEC delete  user by id
 * @params POST /api/v1/admin/users/:id
 * @access PRIVATE admin
 **/
exports.deleteUser = async (req, res) => {
  try {
    // find user
    const userChoosen = await User.findById(req.params.id);
    if (!userChoosen)
      return res.status(404).json({ message: "user not found" });

    //delete user photo profil from Cloudinary
    await RemovePhotoCloudinary(userChoosen.image.publicId);

    // get all posts of user from db
    const posts = await Post.find({ owner: req.params.id });
    // get all public id images of deleted posts
    let arrayImpagesPosts = posts.map((post) => post.image.publicId);
    // remove all images from Cloudinary
    if (arrayImpagesPosts.length) {
      await RemoveMultiplePhotosCloudinary(arrayImpagesPosts);
    }
    // delete user
    const deleteUser = await User.findByIdAndDelete(req.params.id);
    // delete all posts of user
    const deletePosts = await Post.deleteMany({ owner: req.params.id });
    // delete all comments of user
    const deleteComments = await Comment.deleteMany({ owner: req.params.id });

    res.status(200).json({ message: "user has been deleted successfully" });
  } catch (error) {
    res.status(500).json("something wrong");
    console.log(error);
  }
};
