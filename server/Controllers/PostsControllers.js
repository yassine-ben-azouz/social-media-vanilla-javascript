const { Post, ValidatePost } = require("../Models/PostsModel");
const { Comment } = require("../Models/CommentsModel");
const { posts } = require("../data");
const {
  uploadPhotoCloudinary,
  RemovePhotoCloudinary,
} = require("../utils/cloudinary");

/**
 * @DEC create new post
 * @params POST /api/v1/post/
 * @access PRIVATE user logged in
 **/
exports.createPost = async (req, res) => {
  try {
    const { title, description, category } = req.body;
    if (!title || !description || !category)
      return res
        .status(400)
        .json({ message: "please fill all required fields" });

    // validate data from client
    const { error } = ValidatePost(req.body);
    if (error)
      return res.status(400).json({ message: error.details[0].message });
    // check is file image exist or not
    if (!req.file)
      return res.status(400).json({ message: "no image provided" });
    // call uploadPhotoCloudinary function to upload image in Cloudinary
    const ResultCloudinary = await uploadPhotoCloudinary(
      req.file.path, // file upload
      "social_media_vanilla_javascript_posts" // dossier for save photo
    );
    const newPost = await Post.create({
      title,
      description,
      category,
      owner: req.userId,
      image: {
        url: ResultCloudinary.secure_url, // url photo
        publicId: ResultCloudinary.public_id, // public id to remove then call RemovePhotoCloudinary function
      },
    });
    res
      .status(201)
      .json({ message: `create post ${title} successfully`, newPost });
  } catch (error) {
    res.status(500).json({ message: "something wrong" });
    console.log(error);
  }
};

/**
 * @DEC insert many post
 * @params POST /api/v1/admin/post/many
 * @access PRIVATE user admin
 **/
exports.insertManyPosts = async (req, res) => {
  try {
    const newPost = await Post.insertMany([
      {
        title: "The bad guys 2022",
        description:
          "After a lifetime of legendary heists, notorious criminals Mr. Wolf, Mr. Snake, Mr. Piranha, Mr. Shark and Ms. Tarantula are finally caught. To avoid a prison sentence, the animal outlaws must pull off their most challenging con yet -- becoming model citizens.",
        image:
          "https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/c7268c79-9436-4230-b75e-2814495b25f4/dewhoi7-e6ac4474-f551-43ea-94c7-70f3a8aa50bf.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcL2M3MjY4Yzc5LTk0MzYtNDIzMC1iNzVlLTI4MTQ0OTViMjVmNFwvZGV3aG9pNy1lNmFjNDQ3NC1mNTUxLTQzZWEtOTRjNy03MGYzYThhYTUwYmYucG5nIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.m-mjbzn_WGZpijffy0F3dzZXDmEuUjA5EDRkIidx7Is",
        owner: "63eb7ab224f9dc2c8990660c",
      },

      {
        title: "Your Name 2016",
        description:
          "From director Makoto Shinkai, the innovative mind behind Voices of a Distant Star and 5 Centimeters Per Second, comes a beautiful masterpiece about time.",
        image:
          "https://image.yts.mx/assets/images/movies/your_name_2016/medium-cover.jpg",
        owner: "63eb7ab224f9dc2c8990660c",
      },

      {
        title: "A Silent Voice: The Movie 2016",
        description:
          "Shouya Ishida starts bullying the new girl in class, Shouko Nishimiya, because she is deaf. But as the teasing continues, the rest of the class starts to turn on Shouya for his lack of compassion.",
        image:
          "https://image.yts.mx/assets/images/movies/a_silent_voice_2016/medium-cover.jpg",
        owner: "63eb7ab224f9dc2c8990660c",
      },

      {
        title: "Zootopia 2016",
        description:
          "From the biggest elephant to the tiniest shrew, the city of Zootopia is a beautiful metropolis where all animals live peacefully with one another.",
        image:
          "https://images-na.ssl-images-amazon.com/images/S/pv-target-images/0ecb745d03d6656e19c12acc7fe9f7a7ba6336a0f2d2c202aff94a8335f00aae._RI_V_TTW_.jpg",
        owner: "63eb7ab224f9dc2c8990660c",
      },

      {
        title: "The Book of Life 2014",
        description:
          "While visiting a museum, a group of five troubled youth, on detention, are won over by their tour guide, Mary Beth. Part of what wins them over is the story she tells from the Book of Life, which contains all the stories of the world, both true and made-up.",
        image:
          "https://image.yts.mx/assets/images/movies/The_Book_of_Life_2014/medium-cover.jpg",
        owner: "63eba0e9d1bedf67ee2e07ba",
      },

      {
        title: "The Secret World of Arrietty 2010",
        description:
          "In Tokyo, twelve-year-old Shô (Tom Holland), a.k.a. Shou (Ryûnosuke Kamiki), a.k.a. Shawn (David Henrie) has a heart condition and is neglected by his parents.",
        image:
          "https://image.yts.mx/assets/images/movies/Arrietty_2010/medium-cover.jpg",
        owner: "63eba0e9d1bedf67ee2e07ba",
      },

      {
        title: "Tangled 2010",
        description:
          "After receiving the healing powers from a magical flower, the baby Princess Rapunzel is kidnapped from the palace in the middle of the night by Mother Gothel.",
        image:
          "https://image.yts.mx/assets/images/movies/The_Book_of_Life_2014/medium-cover.jpg",
        owner: "63eba0e9d1bedf67ee2e07ba",
      },

      {
        title: "Coco 2017",
        description:
          "Rebellious Miguel, a twelve-year-old Mexican boy and hopeful musician, can't understand the family's continuing ban on all music, especially when his icon and the greatest guitar player ever.",
        image:
          "https://image.yts.mx/assets/images/movies/coco_2017/medium-cover.jpg",
        owner: "63eba0e9d1bedf67ee2e07ba",
      },

      {
        title: "Frozen",
        description:
          "In the Kingdom of Arendelle, Princess Elsa has the power to create and freeze ice and snow, and her younger sister Anna loves to play with her.",
        image:
          "https://image.yts.mx/assets/images/movies/Frozen_2013/medium-cover.jpg",
        owner: "63eba0e9d1bedf67ee2e07ba",
      },

      {
        title: "I Want to Eat Your Pancreas 2017",
        description:
          "One day, Me - a highschooler - found a paperback in the hospital. The Disease Coexistence Journal was its title. It was a diary that M es classmate, Sakura Yamauchi, had written in secret.",
        image:
          "https://image.yts.mx/assets/images/movies/i_want_to_eat_your_pancreas_2018/medium-cover.jpg",
        owner: "63eb7ab224f9dc2c8990660c",
      },

      {
        title: "Ratatouille 2007",
        description:
          "Remy, a provincial rat with a wonderful sense of smell, hates garbage and risks death to enter a human kitchen where he discovers real food and the cooking of five-star chef, Anton Gusteau, author of Anyone Can Cook.",
        image:
          "https://image.yts.mx/assets/images/movies/Ratatouille_2007/medium-cover.jpg",
        owner: "63eb7ab224f9dc2c8990660c",
      },

      {
        title: "Howl's Moving Castle 2004",
        description:
          "Young Sophie Hatter is cursed by the Witch of the Waste, and turns into an old hag. Ashamed of how she looks, she flees into the hills where a moving castle roams the hills.",
        image:
          "https://image.yts.mx/assets/images/movies/howls_moving_castle_2004/medium-cover.jpg",
        owner: "63eb7ab224f9dc2c8990660c",
      },

      {
        title: "Anastasia",
        description:
          "The year is 1916. In St. Petersburg, Russia, Czar Nicholas II (Rick Jones) is throwing a grand ball to celebrate the 300th Anniversary of Romanov rule, while his mother, Dowager Empress Marie Feodorovna (Dame Angela Lansbury).",
        image:
          "https://image.yts.mx/assets/images/movies/anastasia_1997/medium-cover.jpg",
        owner: "63eb7ab224f9dc2c8990660c",
      },

      {
        title: "Whisper of the Heart 1995",
        description:
          "14-year-old teenage bookworm Shizuku lives a simple life, dominated by her love for stories and writing. One day she notices that all the library books she has have been previously checked out by the same person: 'Seiji Amasawa'.",
        image:
          "https://image.yts.mx/assets/images/movies/whisper_of_the_heart_1995/medium-cover.jpg",
        owner: "63eb7ab224f9dc2c8990660c",
      },

      {
        title: "The Godfather 1972",
        description:
          "The story begins as Don Vito Corleone, the head of a New York Mafia family, oversees his daughters wedding with his wife Carmela.",
        image:
          "https://m.media-amazon.com/images/M/MV5BM2MyNjYxNmUtYTAwNi00MTYxLWJmNWYtYzZlODY3ZTk3OTFlXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_.jpg",
        owner: "63eb7ab224f9dc2c8990660c",
      },

      {
        title: "The Dark Knight 2008",
        description:
          "14-year-old teenage bookworm Shizuku lives a simple life, dominated by her love for stories and writing. One day she notices that all the library books she has have been previously checked out by the same person: 'Seiji Amasawa'.Gotham's new District Attorney has been elected. His name is Harvey Dent, and he has a radical new agenda that threatens to take down Gotham's organized crime underworld once and for all with an iron fist.",
        image:
          "https://cdn.europosters.eu/image/750/posters/batman-dark-knight-joker-i116354.jpg",
        owner: "63eb7ab224f9dc2c8990660c",
      },

      {
        title: "Five Feet Apart 2019",
        description:
          "Seventeen-year-old Stella spends most of her time in the hospital as a cystic fibrosis patient. Her life is full of routines, boundaries and self-control all of which get put to the test when she meets Will.",
        image:
          "https://image.yts.mx/assets/images/movies/five_feet_apart_2019/medium-cover.jpg",
        owner: "63eb7ab224f9dc2c8990660c",
      },

      {
        title: "Love, Rosie 2014",
        description:
          "From the age of 5 Rosie Dunne and Alex Stewart have been best friends, as they take on life they just end up getting separated time and time again.",
        image:
          "https://image.yts.mx/assets/images/movies/love_rosie_2014/medium-cover.jpg",
        owner: "63eb7ab224f9dc2c8990660c",
      },

      {
        title: "The Fault in Our Stars 2014",
        description:
          "The seventeen year-old Hazel Grace has lung cancer and needs to carry an oxygen tank wherever she goes. Her mother Frannie encourages her to go to a cancer support group against her will.",
        image:
          "https://image.yts.mx/assets/images/movies/The_Fault_in_Our_Stars_2014/medium-cover.jpg",
        owner: "63eb7ab224f9dc2c8990660c",
      },

      {
        title: "The Devil Wears Prada 2006",
        description:
          "Andy, a young graduate aspiring to be a journalist, comes to New York and becomes an assistant to one of the city's biggest magazine editors, the ruthless and cynical Miranda Priestly.",
        image:
          "https://lumiere-a.akamaihd.net/v1/images/pp_thedevilwearsprada_21373_dabe3cc6.jpeg?region=0%2C0%2C540%2C810",
        owner: "63ee5d369bfce8fcb2d5b2dc",
      },

      {
        title: "Pokémon: Mewtwo Strikes Back 1998",
        description:
          "When a group of scientists are offered funding into genetic research if they agree to try and clone the greatest ever Pokémon.",
        image:
          "https://image.yts.mx/assets/images/movies/pokemon_the_first_movie_mewtwo_strikes_back_1998/medium-cover.jpg",
        owner: "63ee5d369bfce8fcb2d5b2dc",
      },

      {
        title: "Gnomeo & Juliet 2011",
        description:
          "Garden gnomes Gnomeo (James McAvoy) and Juliet (Emily Blunt) have as many obstacles to overcome as their quasi namesakes when they are caught up in a feud between neighbors",
        image:
          "https://image.yts.mx/assets/images/movies/Gnomeo_Juliet_2011/medium-cover.jpg",
        owner: "63ee5d369bfce8fcb2d5b2dc",
      },

      {
        title: "Diary of a Wimpy Kid",
        description:
          "To Greg Heffley, middle school is the dumbest idea ever invented. It's a place rigged with hundreds of social landmines.",
        image:
          "https://image.yts.mx/assets/images/movies/Diary_of_a_Wimpy_Kid_Duology_2010/medium-cover.jpg",
        owner: "63ececefe33637f0d8f86e45",
      },
      {
        title: "Detective Conan: Crimson Love Letter 2017",
        description:
          "A bombing case at Nichiuri TV in autumn. The Satsuki Cup, which crowns the winner of Japan's Hyakunin Isshu, is currently being filmed inside the facility.",
        image:
          "https://image.yts.mx/assets/images/movies/detective_conan_crimson_love_letter_2017/medium-cover.jpg",
      },

      {
        title: "Tom and Jerry: Willy Wonka and the Chocolate Factory",
        description:
          "An animated re-telling of the film Willy Wonka and the Chocolate Factory, in which Tom and Jerry help Charlie Bucket attain a Golden Ticket and secretly accompany him into Willy Wonka's chocolate factory to prevent one of Wonka's competitors from stealing a special candy known as the Everlasting Gobstopper, but also experience the wonderful world of the chocolate factory with the guide of Tuffy.",
        image:
          "https://image.yts.mx/assets/images/movies/tom_and_jerry_willy_wonka_and_the_chocolate_factory_2017/medium-cover.jpg",
        owner: "63ececefe33637f0d8f86e45",
      },

      {
        title: "Wolf Children 2012",
        description:
          "After her werewolf lover unexpectedly dies in an accident while hunting for food for their children, a young woman must find ways to raise the werewolf son and daughter that she had with him while keeping their trait hidden from society.",
        image:
          "https://image.yts.mx/assets/images/movies/wolf_children_2012/medium-cover.jpg",
        owner: "63ececefe33637f0d8f86e45",
      },

      {
        title: "John Wick 2014",
        description:
          "John Wick is an American neo-noir action-thriller media franchise created by screenwriter Derek Kolstad and starring Keanu Reeves as John Wick.",
        image: "https://i.ytimage.com/vi/v_lYz3LKKgM/movieposter_en.jpg",
        owner: "63ececefe33637f0d8f86e45",
      },

      {
        title: "avengers infinity war",
        description:
          "The Avengers must stop Thanos, an intergalactic warlord, from getting his hands on all the infinity stones. However, Thanos is prepared to go to any lengths to carry out his insane plan.",
        image:
          "https://terrigen-cdn-dev.marvel.com/content/prod/1x/avengersinfinitywar_lob_mas_hlf_01_3.jpg",
        owner: "63ececefe33637f0d8f86e45",
      },
    ]);
    res.status(201).json({ message: `create posts successfully`, newPost });
  } catch (error) {
    res.status(500).json({ message: "something wrong" });
    console.log(error);
  }
};

/**
 * @DEC get All posts
 * @params POST /api/v1/post/
 * @access PRIVATE user logged in
 **/
exports.getAllPosts = async (req, res) => {
  const POSTS_PER_PAGE = 3;
  const PostsCount = await Post.count();
  const currentPage =
    req.params.currentPage !== "undefined" ? req.params.currentPage : 1;
  try {
    const posts = await Post.find()
      .sort({ createdAt: -1 })
      .skip((currentPage - 1) * POSTS_PER_PAGE)
      .limit(POSTS_PER_PAGE) // infinite scroll
      .populate("owner")
      .populate("comments");

    const pagesCount = Math.ceil(PostsCount / POSTS_PER_PAGE);

    res.status(200).json({ posts, pagesCount, PostsCount });
  } catch (error) {
    res.status(500).json({ message: "something wrong" });
    console.log(error);
  }
};

/**
 * @DEC get  post
 * @params GET /api/v1/post/:id
 * @access PRIVATE user connected
 **/
exports.getPost = async (req, res) => {
  try {
    const post = await Post.findById(req.params.id)
      .populate("owner", "username image")
      .populate("comments");
    if (!post) return res.status(404).json({ message: "post not found" });
    res.status(200).json(post);
  } catch (error) {
    res.status(500).json({ message: "something wrong" });
    console.log(error);
  }
};

/**
 * @DEC toggle like
 * @params PUT /api/v1/post/like
 * @access PRIVATE owner (user logged in)
 **/
exports.toggleLike = async (req, res) => {
  try {
    let choosenPost = await Post.findById(req.params.id);
    if (!choosenPost)
      return res.status(404).json({ message: "post not found" });

    const checkLike = choosenPost.likes.find(
      (ownerId) => ownerId.toString() === req.userId // check post is already liked or not
    );
    if (checkLike) {
      choosenPost = await Post.findByIdAndUpdate(
        req.params.id,
        {
          $pull: { likes: req.userId }, // remove item from array
        },
        { new: true }
      );
    } else {
      choosenPost = await Post.findByIdAndUpdate(
        req.params.id,
        {
          $push: { likes: req.userId }, // push add item in array
        },
        { new: true }
      );
    }
    res.status(200).json({ message: "toggle like successfully", choosenPost });
  } catch (error) {
    res.status(500).json({ message: "something wrong" });
    console.log(error);
  }
};

/**
 * @DEC update post
 * @params PUT /api/v1/post/
 * @access PRIVATE owner post
 **/
exports.updatePost = async (req, res) => {
  try {
    // validate data from client
    const { title, description, category } = req.body;
    if (!title || !description)
      return res
        .status(400)
        .json({ message: "please fill all required fields" });
    // validate data from client
    const { error } = ValidatePost(req.body);
    if (error)
      return res.status(400).json({ message: error.details[0].message });
    // check is file image exist or not
    if (!req.file)
      return res.status(400).json({ message: "no image provided" });

    //check is post exist or not
    const postChoosen = await Post.findById(req.params.id);
    if (!postChoosen)
      return res.status(404).json({ message: "post not found" });
    // check owner of post choosen
    if (postChoosen.owner.toString() !== req.userId)
      return res
        .status(401)
        .json({ message: "unauthorized, you are not owner of this post" });
    // call uploadPhotoCloudinary function to upload image in Cloudinary
    const ResultCloudinary = await uploadPhotoCloudinary(
      req.file.path, // file upload
      "posts" // dossier for save photo
    );
    // delete old image post from cloudinary
    await RemovePhotoCloudinary(postChoosen.image.publicId);
    // update post choosen
    const updatePost = await Post.findByIdAndUpdate(req.params.id, {
      title,
      description,
      category,
      image: {
        url: ResultCloudinary.secure_url, // url photo
        publicId: ResultCloudinary.public_id, // public id to remove then call RemovePhotoCloudinary function
      },
    });
    // send response data to client
    res
      .status(200)
      .json({ message: `update post ${title} successfully`, updatePost });
  } catch (error) {
    res.status(500).json({ message: "something wrong" });
    console.log(error);
  }
};

/**
 * @DEC delete  post
 * @params DELETE /api/v1/post/:id
 * @access PRIVATE Owner post and amdin
 **/
exports.deletePost = async (req, res) => {
  try {
    const postChoosen = await Post.findById(req.params.id);
    if (!postChoosen)
      return res.status(404).json({ message: "post not found" });
    // check owner of post or is admin
    if (
      req.userRole === "admin" ||
      postChoosen.owner.toString() === req.userId
    ) {
      // delete post
      const deletePost = await Post.findByIdAndDelete(req.params.id);
      // delete comments of post
      const deleteComments = await Comment.deleteMany({
        PostId: req.params.id,
      });
      // delete image post from cloudinary
      await RemovePhotoCloudinary(postChoosen.image.publicId);

      res.status(200).json({
        message: `post ${postChoosen.title} has been deleted successfully`,
        deletePost,
      });
    } else {
      return res.status(401).json({
        message: "unauthorized you are not owner of this post or admin",
      });
    }
  } catch (error) {
    res.status(500).json({ message: "something wrong" });
    console.log(error);
  }
};
