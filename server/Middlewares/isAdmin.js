exports.isAdmin = async (req, res, next) => {
  try {
    if (req.userRole !== "admin")
      res.status(404).json({ message: "you are not admin" });
    next();
  } catch (error) {
    res.status(500).json({ message: "something wrong" });
    console.log(error);
  }
};
