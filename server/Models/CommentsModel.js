const mongoose = require("mongoose");
const Joi = require("joi");
const CommentSchema = mongoose.Schema(
  {
    PostId: {
      type: mongoose.Types.ObjectId,
      ref: "post",
      required: true,
    },
    owner: {
      type: mongoose.Types.ObjectId,
      ref: "user",
      required: true,
    },
    text: {
      type: String,
      required: true,
    },
    username: {
      type: String,
      required: true,
    },
    image: {
      type: String,
      required: true,
    },
  },
  { timestamps: true }
);

const Comment = mongoose.model("comment", CommentSchema);
// validate data for create comment
const ValidateCreateComment = (obj) => {
  const schema = Joi.object({
    PostId: Joi.string().required(),
    text: Joi.string().trim().required(),
  });
  return schema.validate(obj);
};

// validate data for update comment
const ValidateUpdateComment = (obj) => {
  const schema = Joi.object({
    text: Joi.string().trim().required(),
  });
  return schema.validate(obj);
};

module.exports = {
  ValidateCreateComment,
  ValidateUpdateComment,
  Comment,
};
