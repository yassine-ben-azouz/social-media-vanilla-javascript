const mongoose = require("mongoose");
const Joi = require("joi");
const PostSchema = mongoose.Schema(
  {
    title: {
      type: String,
      require: true,
    },
    description: {
      type: String,
      required: true,
      trim: true,
    },
    image: {
      type: Object,
      default: {
        url: "",
        publicId: null,
      },
    },
    owner: {
      type: mongoose.Types.ObjectId,
      ref: "user",
      required: true,
    },
    likes: [
      {
        type: mongoose.Types.ObjectId,
        ref: "user",
      },
    ],

    category: {
      type: [String],
    },
    create: {
      type: Date,
      default: new Date(),
    },
  },
  {
    timestamps: true,
    toJSON: { virtuals: true }, // virtuals
    toObject: { virtuals: true }, // virtuals
  }
);

// populate posts the belongs to this user when he/she get his/her profile
PostSchema.virtual("comments", {
  // comments name of feild
  ref: "comment", // ref comment model
  foreignField: "PostId", // field out in ref model (comment)
  localField: "_id", // filed local id
  // compar field out and filed local and get all fields and set them all in filed name comments to this model
});
const Post = mongoose.model("post", PostSchema);

const ValidatePost = (obj) => {
  const schema = Joi.object({
    title: Joi.string().trim().required(),
    description: Joi.string().trim().required(),
    // category: Joi.array().items(Joi.string()), // check array
    category: Joi.string().trim().required(),
  });
  return schema.validate(obj);
};

module.exports = {
  Post,
  ValidatePost,
};
