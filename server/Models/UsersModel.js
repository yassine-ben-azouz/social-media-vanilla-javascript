const mongoose = require("mongoose");
const Joi = require("joi");
const UserSchema = mongoose.Schema(
  {
    username: {
      type: String,
      trim: true,
      required: true,
      minlength: 2,
    },
    email: {
      type: String,
      trim: true,
      unqie: true,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
    image: {
      type: Object,
      default: {
        url: "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png",
        publicId: null,
      },
    },
    role: {
      type: String,
      enum: ["admin", "customer"],
      default: "customer",
    },
  },
  {
    timestamps: true,
    toJSON: { virtuals: true }, // virtuals
    toObject: { virtuals: true }, // virtuals
  }
);

// populate posts the belongs to this user when he/she get his/her profile
UserSchema.virtual("posts", {
  ref: "post", // ref post model
  foreignField: "owner", // field out in ref model (post)
  localField: "_id", // filed local id
  // compar field out and filed local and set fields in filed name posts to this model
});

const User = mongoose.model("user", UserSchema);
// validate register data from client
const ValdiateRegister = (obj) => {
  const schema = Joi.object({
    username: Joi.string().trim().min(2).max(100).required(),
    email: Joi.string().trim().min(2).max(100).trim().email().required(),
    password: Joi.string().trim().min(6).trim().required(),
    role: Joi.string().trim().trim(),
  });
  return schema.validate(obj);
};

// validate login data from client
const ValidateUpdateUser = (obj) => {
  const schema = Joi.object({
    username: Joi.string().trim().min(2).max(100).required(),
    password: Joi.string().trim().min(6).required(),
  });
  return schema.validate(obj);
};
module.exports = { User, ValdiateRegister, ValidateUpdateUser };
